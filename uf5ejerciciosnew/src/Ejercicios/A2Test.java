package Ejercicios;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class A2Test {	

	@Test
	void testCalculo() {
		assertEquals("SI",A2.calculo(25, 20));
		assertEquals("NO",A2.calculo(30, 26));
		assertEquals("NO",A2.calculo(1000, 100));
		assertEquals("SI",A2.calculo(28, 24));
		
	}
	
	@Test
	void testCalculoOculto() {
		assertEquals("NO",A2.calculo(0, 0));
		assertEquals("NO",A2.calculo(2147483647, 2147483647));
		assertEquals("SI",A2.calculo(-1, -600));
		
		
	}
}

