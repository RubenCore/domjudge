package Ejercicios;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

class A1Test {

	ArrayList<String> test = new ArrayList<String>();
	ArrayList<Integer> numeros = new ArrayList<Integer>();

	@Test
	void casosVisibles() {
		// le paso los numeros a comprobar
		numeros.add(5);
		numeros.add(63);

		for (int j = 0; j < numeros.size(); j++) {
			for (int i = 1; i < 11; i++) {
				test.add(numeros.get(j) + "x" + i + "=" + numeros.get(j) * i);
			}
			assertEquals(test, A1.multiplicacion(numeros.get(j)));
			test.clear();
		}

	}
	
	@Test	
	void casoOcults() {
		// le paso los numeros a comprobar
		numeros.add(0);
		numeros.add(-1);
		numeros.add(2147483647);

		for (int j = 0; j < numeros.size(); j++) {
			for (int i = 1; i < 11; i++) {
				test.add(numeros.get(j) + "x" + i + "=" + numeros.get(j) * i);
			}
			assertEquals(test, A1.multiplicacion(numeros.get(j)));
			test.clear();
		}

	}

}
