package Ejercicios;

import java.util.ArrayList;
import java.util.Scanner;

public class A1 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int casos = sc.nextInt();

		ArrayList<String> prueba = new ArrayList<String>();
		
		for (int i = 0; i < casos; i++) {
			int numero = sc.nextInt();
			prueba = multiplicacion(numero);
			
			for (int j = 0; j < prueba.size(); j++) {
				System.out.println(prueba.get(j));
			}

		}

	}

	public static ArrayList<String> multiplicacion(int numero) {
		ArrayList<String> prueba = new ArrayList<String>();
		for (int i = 1; i < 11; i++) {
			
			prueba.add(numero + "x" + i + "=" + numero * i);

		}

		return prueba;
		
	}

}
