package Ejercicios;

import java.util.Scanner;

public class A2 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		int casos = sc.nextInt();

		for (int i = 0; i < casos; i++) {
			int l = sc.nextInt();
			int a = sc.nextInt();

			System.out.println(calculo(l, a));

		}

	}

	public static String calculo(int l, int a) {
		int area = l * a;

		if (area >= 500 && area <= 750) {
			return "SI";
		} else {
			return "NO";
		}

	}

}
